===============================
TACC Scientific Apps Repository
===============================

The goal of this project is to create a repository of Docker_ images
for scientific applications.  The main repository of images is the
`Docker hub`_.  The work here does not try to replicate such content.
We want to add some features and conveniences for developers to the
subset of applications most commonly used by scientists in the TACC
environment.

The features in the current images are:

- **Included documentation**: we define a standard way to add
  documentation and examples to the image.  An containerized
  application in this framework automatically accepts a set of common
  options, such as ``-h`` for help, ``-e`` for examples of use,
  etc. (see :ref:`using`).

- **Attribution metadata**: an uniform way to specify version,
  authors, license, and other metadata.  This can be retrieved through
  command line options, same as the documentation.

- **Analytics**: the containers can automatically send information
  about the application being run to a central server, to collect
  usage and analytics.  This feature can be disabled, if necessary.

- **Developer friendliness**: we want to make it easy for developers
  to extend the images. The images provide simple ways to translate
  the above features to new images through inheritance. It is easy to
  add background processes and to set the environment for an
  application.

.. note:: currently, the only way to extend images is via
          inheritance.  It means that we cannot use other images (for
          example, an arbitrary image from the `Docker hub`_) to
          complement an existing image from this repository.  This is
          a problem that we want to tackle in some form.

.. _using:

Quickstart
==========

Using a sci-app image
---------------------

The images currently built are hosted at the taccsciapps_ organization
at the `Docker hub`_.

Here is an example of discovery and of usage of the features for the
image `taccsciapps/ipython`:

Running the image with no options displays a basic help of usage:

.. code-block:: bash

   $ docker run -it taccsciapps/bioperl

The help includes the help of each of the sci-apps from which the
image inherits.

Display the common flags for the different types of help with:

.. code-block:: bash

   $ docker run -it taccsciapps/bioperl -h

The flag ``-e`` shows examples of usage that can be copy and pasted
for immediate experimentation. For example:

.. code-block:: bash

   $ docker run -it taccsciapps/bioperl -e

shows how to run a Perl script, included in the image, using the
bioperl library.


Extending a sci-app image
-------------------------

Running the image with the ``-d`` flag shows the documentation
included in the image. In particular, it will show the documentation
for the base image ``taccsciapps/base``, which details how to extend
it.

The Dockerfiles for inspecting and modifying existing images can be
found at the `taccaci Bitbucket repository`_.

.. _Docker hub: http://hub.docker.com
.. _Docker: http://docker.io
.. _taccsciapps: https://registry.hub.docker.com/repos/taccsciapps/
.. _taccaci Bitbucket repository: https://bitbucket.org/taccaci/sci-apps/src
